About this `project_data` folders

Contains the project data in any format that will be process by R. This includes model results, workspaces saved and 
intermediate data files. 
